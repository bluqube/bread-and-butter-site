![alt text](logo.svg "Logo Title Text 1")
# Website Source Code

The new Bread and Butter website **(v2.x)** code features fast and lightweight UI frameworks for React with custom-structured CMS via contentful.

## Frameworks/Tools used

| Framework | Description |
| --------- |:----------- |
| React |A JavaScript library for building user interfaces |
| Tailwindcss | A utility-first CSS framework for rapid UI development |
| Contentful | Contentful provides a content infrastructure for digital teams to power content in websites, apps, and devices.  |
| Pagedraw | Pagedraw is a WYSIWYG editor that generates code for your presentational - aka dumb - Angular/React components. |

## Todos
Todo list is now migrated on Figma.
https://www.figma.com/file/ZwVJ5H1FcKoT0bdOM1kNkA3I/B-and-B-Product-Page

## Development
* Install ```yarn``` [[link](https://yarnpkg.com/lang/en/docs/install)].

* Install ```react-static``` CLI:
    ```bash
    yarn global add react-static
    ```

* Install the project dependencies using ```yarn```:
    ```bash
    yarn
    ```

* Start the dev server:
    ```bash
    yarn start
    ```

* Start coding using the design from Figma as a guide [[link](https://www.figma.com/file/ZwVJ5H1FcKoT0bdOM1kNkA3I/B-and-B-Product-Page)].

## Staging
Developers are required to have a Heroku account to test the website online before deployment.

* Install ```heroku``` CLI [[link](https://devcenter.heroku.com/articles/heroku-cli)]. 

* Login to ```heroku``` using Bluqube's developer account: apps.bluqube@gmail.com or developers.noortech@gmail.com
    ```bash
    heroku login
    ```

* Add all changes to the VCS:
    ```bash
    git add .
    ```

* Commit all changes to the VCS:
    ```bash
    git commit -m<message>
    ```
Push changes to Bitbucket and Heroku to the 2.0 branch:
```bash
git push origin 2.0 && git push heroku 2.0:master
```

Visit Bluqube's [staging page](https://bluqube-web-staging.herokuapp.com/) on Heroku to view the website.

## Deployment
* Build website for production:
    ```bash
    yarn build
    ```

* Deploy to B&B FTP. Production build is under the ```dist``` directory.
Visit http://breadandbutter.ph/
