import client from '../config/Contentful'

export default client.getEntries({
  content_type: 'featuredBlogEntries',
})

