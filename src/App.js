import React, { Component } from 'react'
import { Router } from 'react-static'
import Routes from 'react-static-routes'
// import 'assets/lib/select.css'
import 'animate.css/animate.css'
import 'react-accessible-accordion/dist/fancy-example.css'
import 'rodal/lib/rodal.css'
import './app.css'

//TODO: Remove context components since they are no longer needed
// const SlideMenuContext = React.createContext('SlideMenu')

// class Provider extends Component {
//   state = {
//     open: false,
//   }
//   render () {
//     const { open } = this.state
//     return (
//       <SlideMenuContext.Provider value={{
//         state: this.state,
//         toggleMenu: () => this.setState({ open: !open }),
//       }}>
//         {this.props.children}
//       </SlideMenuContext.Provider>
//     )
//   }
// }

// NOTE: App component directly exported instead
export default class App extends Component {
  render () {
    return (
      <Router>
        <div>
          <div className="content overflow-x-hidden">
            <Routes />
          </div>
        </div>
      </Router>
    )
  }
}


// export default React.forwardRef((props, ref) => (
//   <Provider>
//     <SlideMenuContext.Consumer>
//       {context => <App {...props} context={context} ref={ref} />}
//     </SlideMenuContext.Consumer>
//   </Provider>
// ))
