import React, { Component } from 'react'
import growheader from '../assets/grow-with-us-header-desktop.jpg'
import AwardsSection from '../components/AwardsSection'
import BlogSection from '../components/BlogSection'
import BrochureSection from '../components/BrochureSection'
import CommitmentSection from '../components/CommitmentSection'
import CommunitySection from '../components/CommunitySection'
import FAQSection from '../components/FAQSection'
import FooterSection from '../components/FooterSection'
import GrowTestimoniesSection from '../components/GrowTestimoniesSection'
import Header from '../components/Header'
import Helmet from '../components/Helmet'
import NavBar from '../components/NavBar'
import StoreLocatorSection from '../components/StoreLocatorSection'

export default class Grow extends Component {
  render () {
    return (
      <div>
        <Helmet title="Grow with Us!" />
        <NavBar />
        <Header title="Be part of the fastest growing Bakeshop in the Country!" img={growheader} />
        <CommunitySection />
        <CommitmentSection />
        <GrowTestimoniesSection />
        <AwardsSection />
        <FAQSection />
        <BrochureSection />
        <StoreLocatorSection />
        <BlogSection />
        <FooterSection />
      </div>
    )
  }
}
