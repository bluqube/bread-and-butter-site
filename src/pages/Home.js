import React, { Component } from 'react'

import Almusal from '../api/Almusal'
import Merienda from '../api/Merienda'
import Pasalubong from '../api/Pasalubong'
import SaluSalo from '../api/SaluSalo'
import Testimonies from '../api/Testimonies'
import almusal from '../assets/almusal-illustration.png'
import matte1 from '../assets/matte1.png'
import matte2 from '../assets/matte2.png'
import matte3 from '../assets/matte3.png'
import matte4 from '../assets/matte4.png'
import merienda from '../assets/merienda-illustration.png'
import pasalubong from '../assets/pasalubong-illustration.png'
import salusalo from '../assets/salu-salo-illustration.png'
import BlogSection from '../components/BlogSection'
import BrochureSection from '../components/BrochureSection'
import FooterSection from '../components/FooterSection'
import Helmet from '../components/Helmet'
import HomeHeader from '../components/HomeHeader'
import NavBar from '../components/NavBar'
import ProductSection from '../components/ProductSection'
import StoreLocatorSection from '../components/StoreLocatorSection'
import StoreSection from '../components/StoreSection'
import TestimonialsSection from '../components/TestimonialsSection'
import FeaturedSection from '../components/FeaturedSection'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      almusalItems: [],
      salusaloItems: [],
      pasalubongItems: [],
      meriendaItems: [],
      testimonyItems: [],
    }
  }

  componentDidMount () {
    const app = this
    console.log(this.props)
    SaluSalo.then(val => {
      app.setState({ salusaloItems: val.items })
    })
    Almusal.then(val => {
      app.setState({ almusalItems: val.items })
    })
    Pasalubong.then(val => {
      app.setState({ pasalubongItems: val.items })
    })
    Merienda.then(val => {
      app.setState({ meriendaItems: val.items })
    })
    Testimonies.then(val => {
      for(var key in val.items){
        if(isEmpty(val.items[key].fields)){
          delete val.items[key]
        }
      }
      app.setState({ testimonyItems: val.items })
    })

    function isEmpty(obj) {
      for(var key in obj) {
          if(obj.hasOwnProperty(key))
              return false;
      }
      return true;
  }
  }

  render () {
    const {
      salusaloItems,
      pasalubongItems,
      meriendaItems,
      almusalItems,
      testimonyItems,
    } = this.state
    return (
      <div>
        <Helmet title="Welcome to Bread & Butter!" />

        <NavBar />

        <HomeHeader />

        <FeaturedSection />

        <StoreSection />

        <div id="products" />
        <div>
          <ProductSection
            step
            illustration={salusalo}
            name="Salu-salo"
            scrollPosition={1747}
            desc="Bread and Butter Blowout"
            details="Sabayan ang makulay at masayang salu-salo kasama ang Bread & Butter Joycakes at desserts"
            color="scarlet"
            matte={matte1}
            items={salusaloItems}
          />
          <ProductSection
            step={false}
            illustration={pasalubong}
            name="Pasalubong"
            scrollPosition={2413}
            desc="Bread & Butter Delicacies"
            details="Siguradong damang dama ang iyong alala at ngiting dala kasama ang Bread & Butter delicacies at pasalubong"
            color="plum"
            matte={matte2}
            items={pasalubongItems}
          />
          <ProductSection
            step
            illustration={merienda}
            name="Merienda"
            scrollPosition={3192}
            desc="Bread & Butter Snacks"
            details="Gawing extra special ang merienda ng pamilya at bisita kasama ang makulay at masarap na tinapay mula sa Bread & Butter!"
            color="neon-carrot"
            matte={matte3}
            items={meriendaItems}
          />
          <ProductSection
            step={false}
            illustration={almusal}
            name="Almusal"
            scrollPosition={3870}
            desc="Bread & Butter Breakfast"
            details="Kumpleto ang almusal kasama ang mga paborito niyong tinapay mula sa Bread & Butter!"
            color="tango"
            matte={matte4}
            items={almusalItems}
          />
        </div>

        <BrochureSection downloadURL="http://breadandbutter.ph/downloads/bnb-brochure.pdf" />

        <TestimonialsSection data={testimonyItems} />

        <div id="nearest" />
        <StoreLocatorSection />

        <div id="updates" />
        <BlogSection />

        <div id="franchise" />
        <FooterSection />
      </div>
    )
  }
}

export default Home
