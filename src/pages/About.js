import React, { Component } from 'react'
import Slider from 'react-slick'
import {
  Slide1,
  Slide2,
  Slide3,
  Slide4,
  Slide5,
  Slide6,
  Slide7,
  Slide8,
  Slide9,
  Slide10,
  Slide11,
  Slide12,
} from '../components/Slides/'
import Helmet from '../components/Helmet'
import NavBar from '../components/NavBar'

const sliderConfig = {
  infinite: false,
  dots: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  appendDots: dots => (
    <div
      style={{
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'transparent',
        borderRadius: '10px',
        padding: '10px',
      }}
    >
      <ul style={{ margin: '0px' }}> {dots} </ul>
    </div>
  ),
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
  ],
}

const slides = [
  <Slide1 />,
  <Slide2 />,
  <Slide3 />,
  <Slide4 />,
  <Slide5 />,
  <Slide6 />,
  <Slide7 />,
  <Slide8 />,
  <Slide9 />,
  <Slide10 />,
  <Slide11 />,
  <Slide12 />,
]
class About extends Component {
  render () {
    return (
      <div>
        <div className="h-screen">
          <Helmet title="About Us" />
          <NavBar fixed="true" />
          <Slider {...sliderConfig}>
            {slides.map(i => (
              <div>{i}</div>
            ))}
          </Slider>
        </div>
      </div>
    )
  }
}

export default About
