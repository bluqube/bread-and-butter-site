import React, { Component } from 'react'
import PositionsSection from '../components/PositionsSection'
import FooterSection from '../components/FooterSection'
import ExperienceSection from '../components/ExperienceSection'
import JobInquirySection from '../components/JobInquirySection'
import NavBar from '../components/NavBar'
import Helmet from '../components/Helmet'
import Header from '../components/Header'
import header from '../assets/bnb-careers-header.jpg'

class Careers extends Component {
  render () {
    return (
      <div>
        <Helmet />
        <NavBar />
        <Header title="Experience sweet success with our growing family" img={header} />
        <ExperienceSection />
        <PositionsSection />
        <JobInquirySection />
        <FooterSection />
      </div>
    )
  }
}

export default Careers
