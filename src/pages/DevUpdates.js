import React, { Component } from 'react'
import uuidv4 from 'uuid/v4'
import Header from '../components/Header'
import Helmet from '../components/Helmet'
import NavBar from '../components/NavBar'
import { GitCommit, CheckSquare } from 'react-feather'
import { changelog } from '../config/DevUpdates'
import growheader from '../assets/grow-with-us-header-desktop.jpg'

class DevUpdates extends Component {
  render () {
    return (
      <div>
        <Helmet title="Developer Updates" />
        <NavBar />
        <Header title="</> Developer Updates" img={growheader} />
        <div className="container mx-auto py-8">
          <h2 className="text-scarlet mb-8 font-normal">Welcome team! Listed below are the recent changes to the website for your perusal.</h2>
          {changelog.map(i => (
            <div key={uuidv4()}>
              <h3 className="font-normal">
                <GitCommit className="w-4 h-4 mr-4" />
                {`Version: ${i.version}`}
              </h3>
              <small className="ml-8">{`Released on: ${i.date} `}</small>
              <ul className="mt-2 leading-loose">
                {i.data.map(ia => (
                  <li className="list-reset pl-0" key={uuidv4()}><CheckSquare className="h-4 w-4 mr-2" />{ia.text}</li>
                ))}
              </ul>
              <br />
            </div>
        ))}
        </div>
      </div>
    )
  }
}

export default DevUpdates
