import React, { Component } from 'react'
import { Fade } from 'react-reveal'
import uuidv4 from 'uuid/v4'
import GrowTestimonies from '../api/GrowTestimonies'
import bnbRedRibbon from '../assets/red-ribbon.png'
import GrowTestimonialCard from '../components/GrowTestimonialCard'
import bnb from '../config/Styles'


export default class GrowTestimonialRight extends Component {
  constructor (props) {
    super(props)
    this.state = {
      testimonies: [],
    }
  }
  componentDidMount () {
    const app = this
    GrowTestimonies.then(value => {
      app.setState({ testimonies: value.items })
      console.log(value)
    })
  }

  render () {
    const { testimonies } = this.state
    return (
      <Fade>
        <div className="w-full">
          <div className="container mx-auto">
            <div className={`${bnb.Ribbon} mb-8`} style={{ backgroundImage: `url(${bnbRedRibbon})` }}>Testimonials</div>
          </div>
          {Object.keys(testimonies).map(i => (
            <GrowTestimonialCard
              key={uuidv4()}
              odd={i % 2 === 0}
              color={testimonies[i].fields.color}
              img={testimonies[i].fields.image.fields.file.url}
              name={testimonies[i].fields.name}
              testimony={testimonies[i].fields.testimony}
              matte={testimonies[i].fields.matte.fields.file.url} />
          ))}
        </div>
      </Fade>
    )
  }
}
