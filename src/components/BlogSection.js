import React, { Component } from 'react'
import { Link } from 'react-static'
import { Fade } from 'react-reveal'
import Slider from 'react-slick'
import uuidv4 from 'uuid/v4'

import blog from '../config/Blog'

import bnbYellowRibbon from '../assets/yellow-ribbon.png'
import bnb from '../config/Styles'

const truncate = (str, limit) => {
  let bits = ''
  let i = null

  if (typeof str !== 'string') {
    return ''
  }
  bits = str.split('')
  if (bits.length > limit) {
    for (i = bits.length - 1; i > -1; --i) {
      if (i > limit) {
        bits.length = i
      } else if (bits[i] === ' ') {
        bits.length = i
        break
      }
    }
    bits.push('...')
  }
  return bits.join('')
}


const sliderConfig = {
  infinite: true,
  dots: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
        dots: false,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        initialSlide: 3,
        dots: false,
      },
    },
  ],
}

const Card = props => (
  <Link className="no-underline mx-auto md:flex-1" to={props.blogLink}>
    <div className="max-w-sm rounded overflow-hidden shadow ml-8 mr-8 zoom xs:mb-8">
      <img className="w-full" src={props.coverPhoto} alt="Sunset in the mountains" />
      <div className="px-6 py-4 bg-gold">
        <div className="font-bold text-scarlet mb-2 bnb-card-title">{props.title}</div>
        <p className="text-black-75 text-base bnb-card-desc mb-2">
          {truncate(props.description, 80)}
        </p>
      </div>
    </div>
  </Link>
)

class BlogSection extends Component {
  render () {
    return (
      <Fade>
        <div className="w-full bg-white mb-8">
          <div className={`${bnb.Ribbon} mb-2`} style={{ backgroundImage: `url(${bnbYellowRibbon})` }}>
            <span className="text-quincy">Blog</span>
          </div>
          <div className={bnb.Container}>
            <Slider {...sliderConfig}>
              {blog.map(i => (
                <div key={uuidv4()} className="pt-8">
                  <Card
                    title={i.title}
                    description={i.description}
                    coverPhoto={i.coverPhoto}
                    blogLink={i.blogLink}
                  />
                </div>
              ))}
            </Slider>
            <br />
            <br />
            <br />
            <p className="text-center"><Link className={`${bnb.ButtonDefault} mb-8`} to="https://medium.com/@breadandbutterbakery">More from the Blog</Link></p>
            <br />
          </div>
        </div>
      </Fade>
    )
  }
}

export default BlogSection
