import React, { Component } from 'react'
import { Fade } from 'react-reveal'
import Slider from 'react-slick'

import Demo1 from '../assets/Demo.jpg'

const Demo2 = Demo1
const Demo3 = Demo1

const sliderConfig = {
  infinite: true,
  dots: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  arrows: true,
  autoplaySpeed: 5000,
  appendDots: dots => (
    <div
      style={{
          position: 'absolute',
          bottom: 0,
          backgroundColor: 'transparent',
          borderRadius: '10px',
          padding: '10px',
        }}
      >
      <ul style={{ margin: '0px' }}> {dots} </ul>
    </div>
  ),
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
  ],
}

class FeaturedSection extends Component {
  render () {
    return (
      <Fade>
        <div className="xs:text-center lg:text-left">
          <div className="w-full">
            <Slider {...sliderConfig}>
              <img src={Demo1} alt="" />
              <img src={Demo2} alt="" />
              <img src={Demo3} alt="" />
            </Slider>
          </div>
        </div>
      </Fade>
    )
  }
}

export default FeaturedSection
