import React, { Component } from 'react'
import uuidv4 from 'uuid/v4'
import _ from 'lodash'
import { Fade } from 'react-reveal'
import Slider from 'react-slick'
import async from 'async'
import bnbRedRibbon from '../assets/red-ribbon.png'
import testimonials from '../config/Testimonials'
import bnb from '../config/Styles'

const sliderConfig = {
  infinite: true,
  dots: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        dots: false,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        initialSlide: 3,
        dots: false,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 4,
        dots: false,
      },
    },
  ],
}

const Card = props => (
  <a className="no-underline text-black-75" href="https://www.facebook.com/pg/BreadandButterPhilippines/reviews/" target="_blank" rel="noopener noreferrer">
    <div className="max-w-sm rounded overflow-hidden shadow-md bg-white m-4 mb-0 lg:mx-8 xs:mx-4 xxl:p-bnbxxl-cards">
      <div className="m-8 text-center">
        <img className={bnb.TestimonyAvatar} src={props.picture} alt="Profile" />
        <p className="italic mt-4 xs:my-8">{`"${props.testimony}"`}</p>
        <p className="font-bold mt-8 mb-2">{props.name}</p>
        <p className="text-tango">{props.position}</p>
      </div>
    </div>
  </a>
)

class TestimonialsSection extends Component {
  render () {
    return (
      <Fade>
        <div className="bg-gold pb-8">
          <div className={`${bnb.Ribbon} mb-8`} style={{ backgroundImage: `url(${bnbRedRibbon})` }}>Testimonials</div>
          <Slider {...sliderConfig}>
            {this.props.data.map(i => (
              <div key={uuidv4()}>
                <Card
                  name={i.fields.name}
                  picture={i.fields.image.fields.file.url}
                  position="Customer"
                  testimony={i.fields.testimonies} />
              </div>
                  ))}
          </Slider>
          <br />
        </div>
      </Fade>
    )
  }
}

export default TestimonialsSection
