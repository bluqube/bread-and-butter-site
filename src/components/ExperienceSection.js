import React, { Component } from 'react'
import Masonry from 'react-masonry-component'
import { Slide } from 'react-reveal'
import bnb from '../config/Styles'
import ExperienceGallery from '../api/ExperienceGallery'

class ExperienceSection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      gallery: [],
    }
  }

  componentDidMount () {
    const app = this
    ExperienceGallery.then(value => {
      app.setState({ gallery: value.items })
      console.log(value)
    })
  }
  render () {
    const { gallery } = this.state
    const headerText = (
      <div>
        <h1 className="text-quincy mb-8 xs:text-base lg:text-3xl lg:font-subheader">The Bread & Butter Experience</h1>
        <p className={`${bnb.Paragraph} lg:m-0 xs:mx-4`}>Join our Bread & Butter team and experience the sweet taste of success.</p>
      </div>
    )
    return (
      <div className="w-full lg:p-8 xs:p-2">
        <div className={bnb.Container}>
          <div className="container mx-auto">
            <div className="lg:block xs:hidden">
              <Masonry className="mx-auto" options={{ isFitWidth: true }}>
                <div className="bnb-experience-img h-auto m-4">
                  {headerText}
                </div>
                {gallery.map(i => <img className="bnb-experience-img h-auto m-4" src={i.fields.featuredPhoto.fields.file.url} alt="" />)}
              </Masonry>
            </div>
            <div className="mx-auto xs:block lg:hidden text-center pt-8">
              {headerText}
              {gallery.map(i => <Slide bottom><img className="w-64 h-auto my-4 mx-auto" src={i.fields.featuredPhoto.fields.file.url} alt="" /></Slide>)}
            </div>
            <div className="xs:mx-4 lg:mx-auto mb-8 mx-8 px-8">
              <p className={`${bnb.Paragraph} xxl:px-bnbxxl-position-text`}>
                <span>We consistently provide bakery products grounded on high quality and </span>
                <span>affordability making it one of the major bakeshop </span>
                <span>chains in the Philippines.</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ExperienceSection
