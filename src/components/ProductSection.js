import React, { Component } from 'react'
import { ArrowDown, ArrowUp } from 'react-feather'
import { Fade, Slide } from 'react-reveal'
import _ from 'lodash'
import Slider from 'react-slick'
import { Link } from 'react-static'
import uuidv4 from 'uuid/v4'
import bnbRedRibbon from '../assets/red-ribbon.png'
import bnb from '../config/Styles'


// const SlideArrow = props => (
//   <button
//     onClick={props.onClick}
//     className={props.className}
//     style={{ ...props.style, display: 'block' }}>
//     {props.right&&<ArrowRight className="text-white w-4 h-4 mr-8" />}
//     {props.left&&<ArrowLeft className="text-white w-4 h-4 ml-8" />}
//   </button>
// )

const sliderConfig = {
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 4,
  arrows: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
  ],
}

const Card = props => (
  <Link to="/" className="no-underline p-2">
    <div className="zoom max-w-sm lg:rounded xxl:rounded-lg overflow-hidden shadow-md bg-white m-9">
      <img className="w-full" src={props.coverPhoto} alt={props.name} />
      <div className="px-6 py-4">
        <div className={`font-bold text-xl text-center text-${props.color} xxl:text-bnbxxl-h`}>{props.name}</div>
      </div>
    </div>
  </Link>
)

class ProductSection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showProducts: false,
      showPanel: false,
    }
  }

  toggleProducts = () => {
    this.setState({ showProducts: !this.state.showProducts })
    const { showProducts } = this.state
    if (!showProducts) {
      this.setState({ showPanel: true })
    } else {
      setTimeout(this.setState({ showPanel: false }), 1000)
    }
  }

  render () {
    const { showProducts, showPanel } = this.state
    const product = this.props
    const illustration = (
      <img className="bnb-product-illustration xs:h-auto xs:w-64 sm:w-auto" src={product.illustration} alt={product.desc} />
    )
    const description = (
      <div className="px-8">
        <h1 className={`${bnb.ProductHeaderThemed(product.color)} xs:text-4xl xxl:text-bnbxxl-sh`}>{product.name}</h1>
        <p className="text-black-75 font-subheader text-2xl mt-2 mb-8 xxl:text-bnbxxl-base font-bold">{product.desc}</p>
        <p className={`${bnb.Paragraph} mb-8 lg:text-base xxl:text-bnbxxl-base`}>{product.details}</p>
        <a href={`#${_.snakeCase(product.name)}`}>
          <button
            onClick={() => { this.toggleProducts() }}
            className={`${!showProducts ? bnb.ButtonThemed(product.color) : bnb.ButtonThemedActive(product.color)} mt-8 content-center items-center`}>
            {!showProducts ? <ArrowDown className="w-4 h-4 mr-2" /> : <ArrowUp className="w-4 h-4 mr-2" />}
            <span>{`${showProducts ? 'Hide' : 'View'} Products`}</span>
          </button>
        </a>
      </div>
    )

    return (
      <div>

        <Fade>
          <div className={`${product.step ? 'bg-linen' : 'bg-white'} xs:text-center`}>
            <Fade>
              <div className="w-full">
                {
                  product.name === 'Salu-salo' &&
                  <div className={bnb.Ribbon} style={{ backgroundImage: `url(${bnbRedRibbon})` }}>Our Products</div>
                }
              </div>
            </Fade>
            <div className={`${bnb.Container}`}>
              <div className={`flex ${product.name === 'Salu-salo' ? 'pt-0' : 'pt-8'} xs:hidden lg:flex`}>
                <div className={`w-1/2 ${!product.step ? 'text-center' : 'text-left'}`}>
                  <Slide left>{product.step ? description : illustration}</Slide>
                </div>
                <div className={`w-1/2 ${product.step ? 'text-center' : 'text-right'}`}>
                  <Slide right>{product.step ? illustration : description}</Slide>
                </div>
              </div>
              <div className="sm:hidden">
                {illustration}
                {description}
              </div>
            </div>
          </div>
        </Fade>

        {showPanel &&
          <Fade>
            <div id={_.snakeCase(product.name)} className={`bg-center bg-no-repeat bg-cover ${'z-0'}`} style={{ backgroundImage: `url(${product.matte})` }}>
              <div className="lg:p-4 m-0 bnb-product-card-section xxl:p-bnbxxl-products">
                <Slider {...sliderConfig}>
                  {product.items.map(i => (
                    <div key={uuidv4()}>
                      <Card
                        color={product.color}
                        coverPhoto={i.fields.featuredImage.fields.file.url}
                        name={i.fields.name} />
                    </div>
                  ))}
                </Slider>
              </div>
              <br />
              <div className="clearfix h-px w-full" />
            </div>
          </Fade>
        }
      </div>
    )
  }
}

export default ProductSection
