import React, { Component } from 'react'
import { Fade } from 'react-reveal'

import bnb from '../config/Styles'
import bnbBrochure from '../assets/brochure.png'

class BrochureSection extends Component {
  render () {
    return (
      <Fade>
        <div className="z-50 w-full bg-linen lg:text-left xs:text-center">
          <div className={bnb.Container}>
            <img className="bnb-brochure xs:inline sm:hidden xs:w-72 xs:h-auto" src={bnbBrochure} alt="Pandesal" />
            <div className="lg:flex my-8 xs:my-2 xs:block">
              <div className="lg:w-1/2 xs:w-full lg:pt-8">
                <div className="w-full h-16" />
                <p className="bnb-paragraph text-5xl mt-8 mb-8 pl-0 leading-normal xs:mt-0">
                  <span className="lg:text-2xl xxl:text-bnbxxl-brochure">Download our brochure to see all the products that we offer.</span>
                  <br /><br />
                  <a className={`${bnb.ButtonDefault} mt-4 xs:w-full`} href={this.props.downloadURL}>Download</a>
                </p>
              </div>
              <div className="lg:w-1/2 lg:inline xs:hidden text-right">
                <img className="bnb-brochure xxl:w-bnbxxl-brochure xxl:h-auto" src={bnbBrochure} alt="Pandesal" />
              </div>
            </div>
          </div>
        </div>
      </Fade>
    )
  }
}

export default BrochureSection
