import React, { Component } from 'react'
import { Fade, Zoom } from 'react-reveal'

import bnb from '../config/Styles'
import pinIcon from '../assets/ios-pin.png'
import storeHeader from '../assets/store-header.svg'
import MapModal from '../components/MapModal'

class StoreLocatorSection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
    }
  }

  show = () => {
    this.setState({ visible: true })
  }

  hide = () => {
    this.setState({ visible: false })
  }

  render () {
    const { visible } = this.state
    return (
      <div className="xs:hidden lg:block xxl:block">
        <MapModal
          visible={visible}
          onClose={this.hide}
        />
        <Fade>
          <div className="w-full bg-scarlet">
            <div className="container mx-auto mx-8 lg:p-8 xxl:py-bnbxxl-store text-center">
              <p className="mx-8"><img className="w-10 h-auto animated pulse infinite mt-8" alt="Pin" src={pinIcon} /></p>
              <Zoom><p className="mb-8"><img className="bnb-store-header" alt="Store Header" src={storeHeader} /></p></Zoom>
              <Zoom>
                <div className="w-full max-w-sm mx-auto">
                  <button className={bnb.ButtonInverted} onClick={this.show}>Find Store</button>
                </div>
              </Zoom>
            </div>
          </div>
        </Fade>
      </div>
    )
  }
}

export default StoreLocatorSection
