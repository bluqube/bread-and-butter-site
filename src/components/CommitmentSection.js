import React, { Component } from 'react'
import { Fade } from 'react-reveal'
import hassle from '../assets/bnb_illustration5_hassle.png'
import relationship from '../assets/bnb_illustration5_relationship.png'
import roi from '../assets/bnb_illustration5_roi.png'
// import relationships from '../assets/relationships.png'
import bnb from '../config/Styles'

const relationshipContent = (
  <React.Fragment>
    <span>As we grow together with you, we commit to a relationship </span>
    <span>that aims to be of service to your specific need.</span>
  </React.Fragment>
)

const hassleFreeContent = (
  <React.Fragment>
    <span>To maintain the highest quality, Bread & Butter products are </span>
    <span>constantly produced with the best quality ingredients and are </span>
    <span>delivered fresh daily to your store for you to simply sell.</span>
  </React.Fragment>
)

const roiContent = (
  <React.Fragment>
    <span>With the constant need for quality bread daily at competitive retail </span>
    <span>price, a Bread & Butter investment gives you the best margins for a </span>
    <span>faster return on your investment.</span>
  </React.Fragment>
)

class CommitmentSection extends Component {
  constructor (props) {
    super(props)


    this.state = {
      commitment:
        (
          <React.Fragment>
            <span>As we grow together with you, we commit to a relationship</span>
            <span>that aims to be of service to your specific need.</span>
          </React.Fragment>
        )
      ,
    }
  }

  handleHover = contentToRender => {
    this.setState({ commitment: contentToRender })
  }

  render () {
    const { commitment } = this.state

    return (
      <Fade>
        <div className="bg-linen w-full text-center py-8">
          <div className="container mx-auto">
            <div className="flex my-8 mx-auto bnb-commitment">
              <div className="w-1/3 text-center zoom bnb-commitment--item" onMouseOver={() => this.handleHover(hassleFreeContent)} onFocus={() => this.handleHover(hassleFreeContent)}>
                <img className="lg:h-48 xs:h-16" alt="" src={hassle} />
                <h2 className="text-quincy lg:text-2xl lg:w-48 font-header mx-auto mt-4 xs:text-xs">Hassle Free Bakery</h2>
              </div>
              <div className="w-1/3 text-center zoom bnb-commitment--item" onMouseOver={() => this.handleHover(relationshipContent)} onFocus={() => this.handleHover(relationshipContent)}>
                <img className="lg:h-48 xs:h-16" alt="" src={relationship} />
                <h2 className="text-quincy lg:text-2xl w-64 font-header mx-auto mt-4 xs:text-xs xs:w-auto">Strong and Personal Relationships</h2>
              </div>
              <div className="w-1/3 text-center zoom bnb-commitment--item" onMouseOver={() => this.handleHover(roiContent)} onFocus={() => this.handleHover(roiContent)}>
                <img className="lg:h-48 xs:h-16" alt="" src={roi} />
                <h2 className="text-quincy lg:text-2xl w-48 font-header mx-auto mt-4 xs:text-xs xs:w-auto">Fast ROI</h2>
              </div>
            </div>
          </div>
          <p className={`${bnb.GrowParagraph} bnb-commitment-p mx-auto xs:text-sm xs:px-8`}>
            {commitment}
          </p>
        </div>
      </Fade>
    )
  }
}

export default CommitmentSection
