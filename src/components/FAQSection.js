import React, { Component } from 'react'
import { Fade } from 'react-reveal'

import Accordion from '../components/Accordion'

import faqs from '../api/FAQs'

export default class FAQSection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      content: [],
    }
  }

  componentDidMount () {
    const app = this
    faqs.then(value => {
      app.setState({ content: value.items })
      console.log(value.items)
    })
  }

  render () {
    const { content } = this.state
    return (
      <Fade>
        <div className="py-8">
          <h1 className="text-tango font-header text-center my-8 lg:text-3xl xs:text-base">Frequently Asked Questions</h1>
          <div className="container mx-auto px-8">
            <Accordion data={content || []} />
          </div>
        </div>
      </Fade>
    )
  }
}
