import React, { Component } from 'react'
import bnb from '../config/Styles'

export default class Header extends Component {
  render () {
    return (
      <div className="flex bg-cover bnb-grow-header bg-center" style={{ backgroundImage: `url(${this.props.img})` }}>
        <div className={bnb.Container}>
          <div className="container mx-auto">
            <h1 className="lg:w-3/4 text-white lg:text-5xl xs:text-lg xs:px-8 xs:w-full">{this.props.title}</h1>
          </div>
        </div>
      </div>
    )
  }
}
