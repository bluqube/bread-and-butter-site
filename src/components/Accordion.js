import React, { Component } from 'react'
import sidearrow from '../assets/side-arrow.svg'
import downarrow from '../assets/down-arrow.svg'

class AccordionHeader extends Component {
  render () {
    return (
      <div className="text-quincy lg:text-base xs:text-sm xs:text-left lg:text-left font-bold">
        {this.props.content}
      </div>
    )
  }
}

class AccordionPanel extends Component {
  render () {
    return (
      <div className="animated fadeIn pl-8 lg:py-8 xs:py-2 mb-8">
        {this.props.content}
      </div>
    )
  }
}

class AccordionItem extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
    }
  }
  render () {
    const { open } = this.state
    const { header, body } = this.props
    return (
      <div className="border-b border-grey">
        <button className="w-full flex block pb-8 pt-4" onClick={() => this.setState({ open: !open })} >
          {open?
            <img className="h-4 w-4 mr-4" alt="" src={downarrow} />
          :
            <img className="h-4 w-4 mr-4" alt="" src={sidearrow} />
          }<AccordionHeader content={header} />
        </button>
        {open && <AccordionPanel content={body} />}
      </div>
    )
  }
}


class Accordion extends Component {
  render () {
    const { data } = this.props
    return (
      <div>
        {data.map(i => (
          <AccordionItem key={i} header={i.fields.question} body={i.fields.answer} />
          ))}
      </div>
    )
  }
}

export default Accordion
