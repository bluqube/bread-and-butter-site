import React, { Component } from 'react'
import { Mail, Phone } from 'react-feather'
import uuidv4 from 'uuid/v4'

import contactBackground from '../assets/contact-background.jpg'
import bnbLogo from '../assets/logo-red.svg'
import links from '../config/SocialLinks'
import bnb from '../config/Styles'

class ContactModal extends Component {
  render () {
    return (
      <div className={`${this.props.open ? 'block' : 'hidden'}`}>
        <div className="fixed pin bnb-top-element overflow-auto bg-smoke-light flex">
          <div className="relative animated slideInUp w-full m-auto flex-col flex shadow mx-8">
            <div className="flex bg-white rounded lg:mx-8 sm:mx-2">
              <div className="lg:w-1/3 xs:hidden lg:block bg-gold h-64 p-8 rounded-l bg-cover bg-bottom text-center bnb-contact-sidepane" style={{ backgroundImage: `url(${contactBackground})` }}>
                <img className="w-64 h-auto mb-8 mx-auto" src={bnbLogo} alt="" />
                <p className="text-sm leading-loose"><Phone className="w-4 h-4" /> +639989505227 | 036-500-8989</p>
                <p className="text-sm leading-loose"><Mail className="w-4 h-4" /> info@breadandbutter.ph</p>
                <hr className="border-b border-quincy" />
                <p className="text-center text-scarlet py-4">
                  {links.map(i => (
                    <a key={uuidv4()} target="_blank" rel="noopener noreferrer" href={i.link}>{i.contactIcon}</a>
                ))}
                </p>

              </div>
              <div className="lg:w-2/3 xs:w-full bg-white rounded-r p-8 text-center">
                <h1 className="text-black-75 font-normal mb-8">Get in touch with us</h1>
                <div className="text-left">
                  <input type="text" className="w-full border-b border-grey-dark pb-2 mb-8 bg-transparent" placeholder="Name (i.e. Juan Dela Cruz)" />
                  <input type="text" className="w-full border-b border-grey-dark pb-2 mb-8 bg-transparent" placeholder="Email Address (i.e. juandelacruz@gmail.com) " />
                  <input type="text" className="w-full border-b border-grey-dark pb-2 mb-8 bg-transparent" placeholder="Contact Number (i.e. +639195617582)" />
                  <textarea placeholder="How may we help you?" className="w-full h-12" />
                </div>
                <div className="text-center">
                  <button onClick={() => this.props.toggleMethod()} className={`my-8 ${bnb.ButtonDefault}`}>Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ContactModal
