import React, { Component } from 'react'
import { Fade } from 'react-reveal'
import bnb from '../config/Styles'
import awards from '../assets/awards.png'

export default class AwardsSection extends Component {
  render () {
    return (
      <Fade>
        <div style={{ textAlign: 'center' }}>
          <img src={awards} alt="Awards" className="lg:w-full max-w-sm xs:w-64 mb-8" />
          <h1 className="font-header lg:text-3xl xs:text-base text-quincy mt-8">Multi-Awarded Franchise Brand</h1>
          <p className={`${bnb.Paragraph} lg:w-3/4 mx-auto px-8`}>Bread & Butter has been recognized by numerous
                  national awards for its good business plan,
                  innovative marketing support system, excellent
                  franchise support and regular trainings all
                  towards building a superior Filipino brand.
          </p>
        </div>
      </Fade>
    )
  }
}
