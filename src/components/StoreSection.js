import React, { Component } from 'react'
import { Link } from 'react-static'
import { Fade, Slide } from 'react-reveal'


import bnb from '../config/Styles'

import storefront from '../assets/store-opening-antique.jpeg'


class StoreSection extends Component {
  render () {
    return (
      <Fade>
        <div className="bnb-gradient xs:text-center lg:text-left">
          <div className={bnb.Container}>
            <img className="bnb-storefront-img lg:hidden xs:w-64 xs:h-auto" src={storefront} alt="Bread and Butter Storefront" />
            <div className="w-full flex p-8">
              <div className="w-1/2 xs:w-full lg:w-1/2">
                <Slide left>
                  {/* removed w-64 class from h1 */}
                  <h1 className="text-scarlet font-header lg:text-5xl mb-6 xs:text-2xl sm:w-full xxl:text-bnbxxl-sh">More Than<br />100 Stores<br />Nationwide</h1>
                  <p className="text-quincy font-subheader lg:text-2xl mt-8 xs:hidden lg:inline">Last Store Opening</p>
                  <h3 className="lg:hidden text-quincy font-subheader xxl:text-bnbxxl-h">Last Store Opening</h3>
                  <p className="text-quincy font-header mt-4 mb-8 font-bold xxl:text-bnbxxl-base">Pandan, Antique August 27, 2018</p>
                  <a className={bnb.ButtonDefault} target="_blank" rel="noopener noreferrer" href="https://medium.com/@breadandbutterbakery?source=---------2------------------">Press Release</a>
                </Slide>
              </div>
              <div className="w-1/2 xs:hidden lg:block text-right">
                <Slide right>
                  <img className="bnb-storefront-img xxl:w- mx-auto" src={storefront} alt="Bread and Butter Storefront" />
                </Slide>
              </div>
            </div>
          </div>
        </div>
      </Fade>
    )
  }
}

export default StoreSection
