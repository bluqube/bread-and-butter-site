import React, { Component } from 'react'
import bnb from '../config/Styles'

class JobInquirySection extends Component {
  render () {
    return (
      <div className="w-full p-8">
        <div className="container mx-auto text-center">
          <p className="m-8 lg:text-2xl xs:text-base max-w-sm mx-auto leading-normal text-black-75">Interested? Don’t hesitate to contact us about the job inquiry</p>
          <a className={`${bnb.ButtonDefault} mt-4 xs:w-full lg:w-auto mb-8`} href="mailto:marketing@breadandbutter.ph?Subject=Job%20Inquiry">Inquire</a>
          <br />
          <br />
        </div>
      </div>
    )
  }
}

export default JobInquirySection
