import React, { Component } from 'react'
import { Fade } from 'react-reveal'
import bnb from '../config/Styles'
import ribbonbanner from '../assets/bnb-ribbon-banner.png'

class CommunitySection extends Component {
  render () {
    return (
      <Fade>
        <div className="flex w-full py-8 relative">
          <div className="container mx-auto lg:py-8 xs:py-0">
            <div className={bnb.Container}>
              <div className="flex">
                <div className="w-3/4 lg:py-8 xs:p-0">
                  <h1 className={`${bnb.GrowHeader} xs:hidden lg:block`}>The Community <br /> Bakeshop of Choice</h1>
                  <h1 className={`${bnb.GrowHeader} xs:block lg:hidden xs:text-base ml-4 xs:text-m`}>The Community<br />Bakeshop of Choice</h1>
                  <p className={`${bnb.GrowParagraph} lg:p-0 lg:text-2xl lg:max-w-full xs:text-sm xs:pl-4 xs:max-w-xs xs:pr-8`}>
                    <span>Bread & Butter has grown to more than <span className="text-scarlet font-bold">100 stores</span></span>
                    <span> and is now the fastest growing bakeshop in the country.</span>
                  </p>
                  <p className={`${bnb.GrowParagraph} xs:mb- 0 lg:p-0 lg:text-2xl lg:max-w-full xs:text-sm xs:pl-4 xs:max-w-xs xs:pr-8`}>
                    <span>We consistently provide bakery products grounded on </span>
                    <span>high quality and affordability making it one of the major bakeshop </span>
                    <span>chains in the Philippines.</span>
                  </p>
                </div>
                <div className="w-1/4 relative" />
              </div>
            </div>
          </div>
          <img alt="Ribbon" className={`${bnb.GrowRibbon} xs:w-16 lg:hidden`} src={ribbonbanner} />
          <img alt="Ribbon" className={`${bnb.GrowRibbon} xs:hidden lg:block`} src={ribbonbanner} />
        </div>
      </Fade>
    )
  }
}

export default CommunitySection
