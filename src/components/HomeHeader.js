import React, { Component } from 'react'
import { Zoom, Slide } from 'react-reveal'
import Select from 'react-select'
import bnb from '../config/Styles'
import bnbHomeHeaderBackground from '../assets/home-header-background.jpg'
import bnbHomeHeaderBackgroundMobile from '../assets/header-img--mobile.png'
import bnbWelcomeSectionHeader from '../assets/welcome-section-header.svg'

class HomeHeader extends Component {
  constructor (props) {
    super(props)
    this.state = {
      linkDestination: '#',
    }
  }

  handleChange = selectedOption => {
    if (selectedOption) {
      this.setState({ linkDestination: `#${selectedOption.value}` })
    }
  }
  render () {
    const options = [
      { value: 'franchise', label: 'Inquire about a Franchise' },
      { value: 'products', label: 'Explore our Products' },
      // { value: 'aboutus', label: 'Know What People Say About Us' },
      { value: 'nearest', label: 'Locate the Nearest Store' },
      { value: 'updates', label: 'See the Latest Updates' },
    ]
    const { linkDestination } = this.state
    return (
      <div>
        <div>
          <div className={bnb.HomeHeader} style={{ backgroundImage: `url(${bnbHomeHeaderBackground})` }}>
            <div className="flex my-8 xs:my-4 xs:w-full pt-8">
              <div className="lg:w-3/4 xs:w-full xs:text-center lg:text-left xxl:ml-bnbxxl-wc">
                <Zoom><img className={bnb.WelcomeImage} src={bnbWelcomeSectionHeader} alt="Welcome to Bread and Butter" /></Zoom>
              </div>
              <div className="lg:w-1/4 leading-normal p-6 xs:hidden lg:block xxl:ml-0 xxl:px-0">
                <Slide right>
                  <p>
                    <span className="text-quincy font-bold text-base text-2xl xxl:text-bnbxxl-h">My Name is</span><br />
                    <span className="text-scarlet font-bold text-2xl xxl:text-bnbxxl-h">Baker Betty</span>
                  </p>
                  <div className="mt-4">
                    <span className="text-quincy font-bold text-base xxl:text-bnbxxl-base">How can I help you?</span>
                    <div className="flex">
                      <div className="inline-block relative w-64">
                        <Select label="Options" options={options} styles={bnb.SelectHeader} onChange={this.handleChange} />
                      </div>
                      <Zoom><a className={bnb.ButtonGo} href={linkDestination}>Go</a></Zoom>
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        </div>
        <div className="bnb-mobile-header--home lg:hidden xs:block" style={{ height: '25rem' }} >
          <div className="text-center bg-center bg-no-repeat bg-gold pt-8" style={{ backgroundImage: `url(${bnbHomeHeaderBackgroundMobile})`, height: '30rem' }}>
            <Zoom><img className="bnb-welcome-section-header--mobile" src={bnbWelcomeSectionHeader} alt="Welcome to Bread and Butter" /></Zoom>
          </div>
        </div>
      </div>
    )
  }
}

export default HomeHeader
