import React, { Component } from 'react'
import { Link } from 'react-static'
import uuidv4 from 'uuid/v4'
import links from '../config/SocialLinks'
import bnbLogoRed from '../assets/logo-red.svg'
import bnb from '../config/Styles'


class FooterSection extends Component {
  render () {
    return (
      <footer className="w-full bg-gold py-8 xs:text-center lg:text-left">
        <div className={bnb.Container}>
          <Link to="/"><img className="w-64 h-auto mb-8 xs:w-48 lg:w-64" alt="Bread and Butter" src={bnbLogoRed} /></Link>
          <div className="flex mb-4 xs:block xs:px-4 lg:flex">
            <div className="w-1/3 mr-8 xs:w-full lg:w-1/3 lg:mr-8">
              <h3 className="text-quincy font-bold mb-4 text-2xl">Our Mission</h3>
              <p><small className="text-quincy leading-loose">We emanate the Filipino values of joy, care, thoughtfulness and hospitality through providing safe and quality products to every Filipino home in locations within their reach.</small></p>
            </div>
            <div className="w-1/3 mx-8 xs:w-full xs:m-0 xs:px-2 lg:w-1/3 lg:mr-8">
              <h3 className="text-quincy font-bold mb-4 mx-8 xs:mt-8 lg:mt-0 text-2xl">Say Hello</h3>
              <a className={`${bnb.ButtonFooter} mx-8 xs:w-full xs:m-0 lg:mx-8 lg:w-auto`} href="mailto:marketing@breadandbutter.ph?Subject=Website%20Inquiry">Contact Us</a>
            </div>
            <div className="w-1/3 xs:w-full lg:w-1/3">
              <h3 className="text-quincy font-bold mb-4 xs:mt-8 lg:mt-0 text-2xl">Let's Get Social</h3>
              {links.map(i => (
                <a key={uuidv4()} target="_blank" rel="noopener noreferrer" href={i.link}>{i.icon}</a>
                ))}
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default FooterSection
