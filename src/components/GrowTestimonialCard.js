import React, { Component } from 'react'
import bnb from '../config/Styles'

class GrowTestimonialCard extends Component {
  render () {
    const {
      odd,
      name,
      testimony,
      color,
      img,
      matte,
    } = this.props
    return (
      <div className={`${odd ? 'bg-white' : 'bg-linen'} w-auto lg:p-8 xs:p-0 xs:m-0`}>
        <div className="lg:p-8 xs:p-2">
          <div className="lg:block xs:hidden shadow rounded bg-cover bg-center bg-top bg-no-repeat m-8 bnb-testimonial-card relative p-8" style={{ backgroundImage: `url(${matte})` }}>
            <div className={`bg-cover bg-top ${odd?'bnb-grow-testimonial-img--left':'bnb-grow-testimonial-img--right'}`} style={{ backgroundImage: `url(${img})` }} />
            <div>
              <div className="p-4 relative">
                <div className={`shadow-lg rounded bg-white w-1/3 p-8 absolute ${odd ? 'pin-r' : 'pin-l'} m-8`}>
                  <h1 className={`text-${color} mb-4 mt-4`}>{name}</h1>
                  <p className={bnb.GrowParagraph}>"{testimony}"</p>
                </div>
              </div>
            </div>
          </div>
          <div style={{ backgroundImage: `url(${matte})`, height: '24rem' }} className="xs:block lg:hidden shadow rounded bg-cover bg-no-repeat m-4">
            <div className="relative">
              <div className="bg-center bg-top bg-cover bnb-grow-testimonial-img--mobile" style={{ backgroundImage: `url(${img})` }} />
              <div className="h-32 w-full" />
              <div className="p-4 absolute pin-l pin-r">
                <div className="shadow-lg p-4 rounded bg-white lg:pt-8 xs:pt-4">
                  <h1 className={`text-${color} mb-4 mt-4 lg:text xs:text-base lg:text-left xs:text-center`}>{name}</h1>
                  <p className={`${bnb.GrowParagraph} lg:text-left xs:text-center`}>"{testimony}"</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default GrowTestimonialCard
