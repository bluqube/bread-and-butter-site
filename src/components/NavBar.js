import React, { Component } from 'react'
import provideScrollPosition from 'react-provide-scroll-position'
import { Link } from 'react-static'
import uuidv4 from 'uuid/v4'
import brandLogo from '../assets/bnb-ribbon-banner-nav.png'
import menu from '../config/NavbarMenu'
import bnb from '../config/Styles'
import ContactModal from './ContactModal'

class NavBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      openContact: false,
    }
    this.toggleMenu = this.toggleMenu.bind(this)
  }

  // TODO: Call this for the mobile menu toggle
  toggleMenu () {
    console.log('Clicked')
    this.setState({ open: !this.state.open })
  }

  toggleContact = () => {
    this.setState({ openContact: !this.state.openContact })
  }

  render () {
    const { openContact } = this.state
    return (
      <div>
        <div className="relative">
          <ContactModal open={openContact} toggleMethod={this.toggleContact} />
          <div className="fixed w-full z-50 xxl:w-full">
            <nav className={bnb.NavBar}>
              <div className="container">
                {/* TODO: Add burger menu component here */}
                {/* NOTE: Burger menu should be hidden on screens wider than tablet screen size*/}                
                <Link to="/">
                  <img className={`${bnb.NavBarLogo} lg:block z-50`} src={brandLogo} alt="Bread and Butter Brand" />
                </Link>
              </div>
              <div className={bnb.NavBarContainer}>
                <div className="lg:flex-grow" />
                <div>
                  {menu.map(i => {
                    const component = !i.cta ?
                      (
                        <Link key={uuidv4()} to={i.path} className={`${i.cta ? bnb.NavBarLinkCTA : bnb.NavBarLink} xs:hidden`} target={i.target} rel={i.rel}>
                          {i.name}
                        </Link>
                      )
                      :
                      (
                        <button onClick={() => this.toggleContact()} key={uuidv4()} to={i.path} className={`${i.cta ? bnb.NavBarLinkCTA : bnb.NavBarLink} xs:hidden`}>
                          {i.name}
                        </button>
                      )
                    return component
                  }
                  )}
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}

const NavBarScrollWrapper = ({ scrollTop, scrollLeft }) => (
  <NavBar scrollPosition={scrollTop} scrollLeft={scrollLeft} />
)

const ScrollComponent = provideScrollPosition(NavBarScrollWrapper)

export default ScrollComponent
