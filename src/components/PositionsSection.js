import React, { Component } from 'react'
import uuidv4 from 'uuid/v4'
import Accordion from '../components/Accordion'
import careers from '../api/Careers'
import bnb from '../config/Styles'

class PositionsSection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      careers: [],
      convertedCareerContent: [],
    }
  }

  componentDidMount () {
    const app = this
    const convertedCareerBucket = []
    careers.then(value => {
      const items = value.items
      app.setState({ careers: items })
      items.map(i => {
        convertedCareerBucket.push({
          fields: {
            question: i.fields.title,
            answer: i.fields.careerDetails,
          },
        })
        return null
      })
      app.setState({ convertedCareerContent: convertedCareerBucket })
    })
  }
  render () {
    const { careers, convertedCareerContent } = this.state
    return (
      <div className="w-full bg-linen">
        <div className={bnb.Container}>
          <div className="container mx-auto py-8">
            <h1 className="text-tango font-header text-center my-8 lg:text-5xl xs:text-base xxl:mb-bnbxxl">Open Positions</h1>
            <div className="xs:hidden lg:block">
              {careers.map(i => (
                <div className="flex mb-8 xxl:mb-bnbxxl" key={uuidv4()}>
                  <div className="w-1/3 pl-8">
                    <h3 className="text-quincy pl-8">{i.fields.title}</h3>
                  </div>
                  <div className="w-2/3">
                    <p className="leading-normal">{i.fields.careerDetails}</p>
                  </div>
                </div>
              ))}
            </div>
            <div className="lg:hidden xs:block px-8">
              <Accordion data={convertedCareerContent || []} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default PositionsSection
