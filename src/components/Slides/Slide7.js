import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide7.jpg'
import Trophy2 from '../../assets/slides/trophy2.png'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="lg:pr-8 xs:p-0 bnb-trophy-section mx-auto">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full lg:p-8 xs:p-0 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">Presidential Award for Outstanding Enterprise in Visayas</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                    On 2010, Bread & Butter received the Presidential Awards for Outstanding Micro, Small and Medium enterprises (MSMEs) awarded by Her Excellency President Gloria Macapagal- Arroyo at Malacanan Palace, Manila Philippines.
                    </p>
                  </div>
                  <div className="w-1/2 lg:block xs:hidden py-8 text-center">
                    <img className="bnb-trophy-2 mx-auto" src={Trophy2} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
