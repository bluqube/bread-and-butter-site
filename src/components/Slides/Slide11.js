import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide11.jpg'
import Map from '../../assets/slides/map3.png'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="pr-8">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full py-8 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">Opened 100<sup>th</sup> Bread & Butter Store in Banga, Aklan</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                      Bread & Butter started in <span className="font-bold">1985</span> in the small and relatively remote town of Kalibo, Aklan in the Visayas. From serving traditional Filipino bread, it has grown to serve a wide variety of bread, pastries and cakes.
                    </p>
                  </div>
                  <div className="w-1/2 p-8">
                    <img src={Map} className="m-8 bnb-map3" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
