import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide2.jpg'
import Map from '../../assets/slides/map1.svg'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="pr-8">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full py-8 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">Humble Begginings</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                      In the year <span className="font-bold">1985</span>, Bread & Butter - a small bakery selling morning Pandesal and Hot Spanish in the afternoon located at Osmenia Avenue, Kalibo, Aklan. 
                    </p>                    
                  </div>
                  <div className="lg:w-1/2 lg:block xs:hidden xs:w-full p-8">
                    <img src={Map} className="m-8 bnb-map" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
