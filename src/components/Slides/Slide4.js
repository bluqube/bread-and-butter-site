import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide4.jpg'
import StoreFront from '../../assets/storefront.png'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="pr-8">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full py-8 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">Ventured Into Franchising</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                    On 2003, the company ventured into franchising business and opened its 1st franchised store in Kalibo, Aklan, and continuously expand in the entire region of Panay.
                    </p>
                  </div>
                  <div className="w-1/2 p-8 lg:block xs:hidden">
                    <img src={StoreFront} className="m-8" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
