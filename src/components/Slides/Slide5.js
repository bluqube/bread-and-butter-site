import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide5.jpg'
import Trophy from '../../assets/slides/trophy.png'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="lg:pr-8 xs:p-2 bnb-trophy-section mx-auto">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="w-1/2 py-8 text-right lg:block xs:hidden">
                    <img className="bnb-trophy" src={Trophy} alt="" />
                  </div>
                  <div className="lg:w-1/2 xs:w-full lg:p-8 xs:p-0 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl xs:mt-8">Entrepreneur Philippines Awards in 2006</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                    On 2006, Bread & Butter won the Entrepreneur Philippines Awards as Best in Franchising Support.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
