import React, { Component } from 'react'
import SlideBG from '../../assets/slides/slide3.jpg'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="pr-8">
                <div className="lg:flex xs:inline mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full py-8" />
                  <div className="lg:w-1/2 xs:w-full lg:py-8 xs:py-4 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:mt-8 lg:text-3xl xs:text-2xl">Commissary Operations Opened</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                      Bread & Butter through its humble beginnings and innovative dreams continued to emanate the Filipino values of Joy, Care and Thoughtfulness in every Filipino family celebrations. Making it as most preferred brand in the countryside communities.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
