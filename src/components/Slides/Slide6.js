import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide6.jpg'
import Plaque from '../../assets/slides/plaque.png'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="lg:h-12 xs:h-2 w-full" />
              <img src={Plaque} className="lg:w-64 xs:w-32 m-auto" alt="" />
              <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">Small Medium<br />Enterprise Model</h1>
              <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                Bread & Butter started in <span className="font-bold">1985</span> in the small and relatively remote town of Kalibo, Aklan in the Visayas. From serving traditional Filipino bread, it has grown to serve a wide variety of bread, pastries and cakes.
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
