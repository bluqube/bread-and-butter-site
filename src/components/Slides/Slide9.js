import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide9.jpg'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="lg:pr-8 xs:p-0">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full lg:p-8 xs:p-0 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl xs:mt-8">Won Multiple Awards</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                      Bread & Butter started in <span className="font-bold">1985</span> in the small and relatively remote town of Kalibo, Aklan in the Visayas. From serving traditional Filipino bread, it has grown to serve a wide variety of bread, pastries and cakes.
                    </p>
                  </div>
                  <div className="w-1/2 py-8 xs:hidden lg:block" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
