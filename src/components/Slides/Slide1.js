import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide1.jpg'
import Welcome from '../../assets/slides/welcome.svg'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
        <div className={bnb.Container}>
          <div className="container mx-auto text-center p-8">
            <div className="mt-8" />
            <div className="lg:h-2 xs:h-24 w-full" />
            <img src={Welcome} className="my-8 mx-auto" alt="Bread and Butter Story" />
            <div className="lg:h-2 xs:h-24 w-full" />
            <h3 className="text-white my-8 lg:text-2xl xs:text-base">Swipe to start &gt;&gt; </h3>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
