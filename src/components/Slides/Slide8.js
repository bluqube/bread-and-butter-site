import React, { Component } from 'react'

import SlideBG from '../../assets/slides/slide8.jpg'
import Map from '../../assets/slides/map2.svg'
import bnb from '../../config/Styles'

class Slide extends Component {
  render () {
    return (
      <div>
        <div className="h-screen w-full bg-cover" style={{ backgroundImage: `url(${SlideBG})` }}>
          <div className={bnb.Container}>
            <div className="container mx-auto text-center p-8">
              <div className="lg:pr-8 xs:p-0">
                <div className="flex mb-4 lg:mr-8 xs:mx-2">
                  <div className="lg:w-1/2 xs:w-full py-8 text-left">
                    <div className="lg:h-12 xs:h-2 w-full" />
                    <h1 className="text-white mb-8 lg:text-3xl xs:text-2xl">First Stores in Oriental Mindoro</h1>
                    <p className="text-white leading-normal mb-8 lg:text-2xl xs:text-xs">
                      In the same year, Bread & Butter opened its first branch in Luzon in the neighbouring province of Oriental Mindoro.
                    </p>
                  </div>
                  <div className="w-1/2 p-8 lg:block xs:hidden text-center">
                    <img src={Map} className="mx-auto bnb-map2" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slide
