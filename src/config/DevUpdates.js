import React from 'react'

const changelog = [
  {
    version: '2.1.1',
    date: '07/02/2018',
    data: [
      { text: 'Performed code cleanup on the Grow with Us page. ' },
      { text: 'Replaced accordion with custom component. ' },
      { text: 'Completed data model for BnB posts. ' },
      { text: 'Added initial content for BnB website database. ' },
      { text: 'Improved website responsiveness on Grow with us page. ' },
      { text: 'Improved mobile card for BnB Grow page. ' },
    ],
  },
  {
    version: '2.1.0',
    date: '06/18/2018',
    data: [
      { text: <div>Completed the home page desktop UI.<div /><img alt="" className="w-64 h-auto" src={require('../assets/devupdates/1.png')} /><br /></div> },
      { text: 'Completed the home page mobile UI.' },
      { text: 'Created the BNB data model to contentful. ' },
      { text: 'Completed the BNB blog section and added recent posts.' },
      { text: 'Added code for social media optimization.' },
      { text: 'Completed footer section (responsive)' },
      { text: 'Added animation and transitions support.' },
      { text: 'Completed dynamic toggle for BNB products.' },
      { text: 'Added brochure download link.' },
      { text: 'Added testimonial mock API to the system' },
      { text: 'Completed responsive section for BNB products' },
      { text: 'Added Store locator section to the page.' },
      { text: 'Changed banner icon to "B" in the Navbar.' },
      { text: 'Completed responsive NavBar' },
      { text: 'Revised select component in the home page and added styles.' },
      { text: 'Added an API in communicating with contentful.' },
      { text: 'Fixed blog section responsiveness bug.' },
      { text: 'Fixed brochure section responsiveness bug.' },
      { text: 'Added purgecss for build optimization.' },
      { text: 'Added initial support for dynamic accordion.' },
      { text: 'Grow Page at 80%' },
      { text: 'Added initial support for FAQs section.' },
      { text: 'Removed Products from navbar.' },
      { text: 'Fixed BNB links.' },
    ],
  },
]

export { changelog }
