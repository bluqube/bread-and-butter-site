import React from 'react'
import { Facebook, Twitter, Instagram } from 'react-feather'
import bnb from './Styles'

export default [
  {
    name: 'Facebook',
    link: 'http://www.facebook.com/breadandbutterphilippines/',
    icon: <Facebook className={bnb.SocialIcons} />,
    contactIcon: <Facebook className="w-6 h-6 mx-4 zoom" />,
  },
  {
    name: 'Instagram',
    link: 'http://www.facebook.com/breadandbutterphilippines/',
    icon: <Instagram className={bnb.SocialIcons} />,
    contactIcon: <Instagram className="w-6 h-6 mx-4 zoom" />,
  },
  {
    name: 'Twitter',
    link: 'http://www.facebook.com/breadandbutterphilippines/',
    icon: <Twitter className={bnb.SocialIcons} />,
    contactIcon: <Twitter className="w-6 h-6 mx-4 zoom" />,
  },
]
