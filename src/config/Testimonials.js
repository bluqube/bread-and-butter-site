import axios from 'axios'

const usersAPI = axios.get('https://randomuser.me/api/?results=10&inc=name,picture')

export default usersAPI
