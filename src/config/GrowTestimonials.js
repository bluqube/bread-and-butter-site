import testimonial1 from '../assets/testimonial1.jpg'
import testimonial2 from '../assets/testimonial2.jpg'
import testimonial3 from '../assets/testimonial3.jpg'

export default [
  {
    name: 'Reyes Family',
    date: '',
    image: testimonial1,
    testimony: 'Kumpleto ang almusal kasama ang mga paborito niyong tinapay mula sa Bread & Butter!',
    color: 'tango',
  },
  {
    name: 'John Gomez',
    date: '',
    image: testimonial2,
    testimony: 'Kumpleto ang almusal kasama ang mga paborito niyong tinapay mula sa Bread & Butter!',
    color: 'dark-golden-rod',
  },
  {
    name: 'Salazar Family',
    date: '',
    image: testimonial3,
    testimony: 'Kumpleto ang almusal kasama ang mga paborito niyong tinapay mula sa Bread & Butter!',
    color: 'plum',
  },
]
