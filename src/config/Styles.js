const DropdownStyles = {
  control: (base, state) => ({
    ...base,
    ':active': {
      border: '2px solid #5C3A28',
    },
    ':hover': {
      border: '2px solid #906046',
    },
    border: '2px solid #5C3A28',
    backgroundColor: 'transparent',
    color: '#5C3A28',
    fontSize: '120%',
  }),
  option: (styles, {
    isDisabled,
    isFocused,
    isSelected,
  }) => ({
    ...styles,
    borderBottom: '1px solid #5C3A28',
    ':last-child': {
      border: 'none',
    },
    backgroundColor: isDisabled
      ? null
      : isSelected ? '#FFD600' : isFocused ? '#FFEB82' : null,
    color: isDisabled
      ? '#ccc'
      : isSelected
        ? '#5C3A28'
        : '#5C3A28',
    cursor: isDisabled ? 'not-allowed' : 'default',
  }),
  input: styles => ({ ...styles }),
  placeholder: styles => ({ ...styles }),
  dropdownIndicator: styles => ({
    ...styles,
    color: '#5C3A28',
    ':hover': {
      color: '#906046',
    },
  }),
  indicatorSeparator: styles => ({
    ...styles,
    backgroundColor: '#5C3A28',
    ':hover': {
      color: '#906046',
    },
  }),
}

const InvertedDropdownStyles = {
  control: (base, state) => ({
    ...base,
    ':active': {
      border: '2px solid #FFF',
    },
    ':hover': {
      border: '2px solid #FFF',
    },
    border: '2px solid #FFF',
    backgroundColor: 'transparent',
    color: '#FFF',
    fontSize: '120%',
  }),
  option: (styles, {
    isDisabled,
    isFocused,
    isSelected,
  }) => ({
    ...styles,
    borderBottom: '1px solid #FFF',
    ':last-child': {
      border: 'none',
    },
    backgroundColor: isDisabled
      ? null
      : isSelected ? '#FFD600' : isFocused ? '#FFEB82' : null,
    color: isDisabled
      ? '#fff'
      : isSelected
        ? '#fff'
        : '#333',
    cursor: isDisabled ? 'not-allowed' : 'default',
  }),
  input: styles => ({ ...styles, color: '#fff' }),
  placeholder: styles => ({ ...styles }),
  dropdownIndicator: styles => ({
    ...styles,
    color: '#FFF',
    ':hover': {
      color: '#fff',
    },
  }),
  indicatorSeparator: styles => ({
    ...styles,
    backgroundColor: '#fff',
    ':hover': {
      color: '#fff',
    },
  }),
  valueContainer: styles => ({
    ...styles,
    color: '#FFF',
  }),
}

export default {
  // Always Set in Alphabetical Order
  ButtonDefault: 'border-2 border-scarlet bg-scarlet text-white py-2 px-4 no-underline font-bold hover:bg-white hover:text-scarlet shadow font-default rounded xxl:text-2xl xxl:px-8 xxl:py-4 zoom',
  ButtonFooter: 'border-2 border-quincy bg-transparent p-2 rounded font-bold text-quincy hover:bg-white no-underline zoom',
  ButtonGo: 'py-2 text-quincy no-underline font-bold inline-block mx-4 hover:text-raw-sand mt-0 xxl:text-2xl',
  ButtonMapGo: 'text-white font-bold inline-block mx-4 mt-0 xxl:text-2xl zoom',
  ButtonInverted: 'py-2 px-4 text-scarlet bg-white font-bold inline-block mx-4 mt-0 shadow font-default rounded xxl:text-2xl xxl:px-8 xxl:py-4 zoom',
  ButtonThemed: color => `border-2 border-${color} bg-${color} text-white p-4 no-underline font-bold hover:bg-white hover:text-${color} shadow font-default rounded xxl:text-2xl`,
  ButtonThemedActive: color => `border-2 border-${color} bg-white text-${color} p-4 no-underline font-bold shadow font-default rounded xxl:text-2xl`,
  Container: 'lg:mx-auto py-8 xxl:px-bnbxxl-section-x xxl:py-bnbxxl-section-y',  
  // NOTE: container class breaks the divs' center alignment
  // Container: 'container lg:mx-auto py-8 xxl:px-bnbxxl-section-x xxl:py-bnbxxl-section-y',  
  GrowHeader: 'font-header text-quincy text-header bnb-community mb-8',
  GrowParagraph: 'text-black-75 lg:text-2xl leading-normal mb-8',
  GrowRibbon: 'absolute pin-t bnb-grow-ribbon',
  HomeHeader: 'flex bg-gold lg:block bg-no-repeat bg-cover py-8 xs:hidden',
  NavBar: 'flex items-center justify-between flex-wrap bg-wheat p-0 shadow',
  NavBarContainer: 'w-full block flex-grow lg:flex lg:items-center lg:w-auto',
  NavBarLink: 'block lg:inline-block text-raw-sand hover:text-dark-golden-rod p-4 no-underline font-bold font-default xxl:text-base',
  NavBarLinkCTA: 'block bg-scarlet lg:inline-block text-white p-4 no-underline font-bold font-default zoom',
  NavBarLogo: 'absolute pin-t navbarlogo zoom xs:navbarlogo-mobile xxl:w-ribbonxxl xxl:ml-ribbonxxl',
  Paragraph: 'text-black-75 font-subheader mt-8 leading-normal bnb-paragraph xs:text-sm lg:text-2xl',
  ProductHeaderThemed: color => `text-${color} font-header text-5xl mt-8`,
  Ribbon: 'text-center text-white pt-6 px-8 pb-8 font-bold bg-no-repeat bg-center bg-contain sm:pt-6 sm:h-24 sm:text-2xl xs:text-base xs:p-2 xs:pt-4 xs:h-16',
  SelectHeader: DropdownStyles,
  MapDropDown: InvertedDropdownStyles,
  SocialIcons: 'w-8 h-8 mr-4 text-quincy hover:text-raw-sand',
  TestimonyAvatar: 'rounded-full mx-auto w-32 h-32 xxl:w-bnbxxl-testimony-avatar xxl:h-auto',
  WelcomeImage: 'lg:m-8 bnb-welcome-section-header lg:mx-0 lg:w-1/2 lg:ml-bnbxxl-wc lg:mb-bnblg-wc xs:w-64 xs:mx-auto xxl:w-bnbxxl-wc xxl:mt-bnbxxl-wc xxl:ml-0 xxl:ml-8 xxl:mb-bnbxxl-h',
}
