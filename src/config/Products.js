import salusalo from '../assets/salu-salo-illustration.png'
import pasalubong from '../assets/pasalubong-illustration.png'
import merienda from '../assets/merienda-illustration.png'
import almusal from '../assets/almusal-illustration.png'
import matte1 from '../assets/matte1.png'
import matte2 from '../assets/matte2.png'
import matte3 from '../assets/matte3.png'
import matte4 from '../assets/matte4.png'
import cover1 from '../assets/cover1.jpg'
import cover2 from '../assets/cover2.jpg'
import cover3 from '../assets/cover3.jpg'
import cover4 from '../assets/cover4.jpg'

export default [
  {
    name: 'Salu-salo',
    desc: 'Bread and Butter Blowout',
    details: 'Sabayan ang makulay at masayang salu-salo kasama ang Bread & Butter Joycakes at desserts',
    matteImage: matte1,
    illustration: salusalo,
    color: 'scarlet',
    featuredItems: [
      {
        name: 'Piyaya',
        coverPhoto: cover1,
      },
      {
        name: 'Wheat Pandesal',
        coverPhoto: cover2,
      },
      {
        name: 'Cinnamon Toast',
        coverPhoto: cover3,
      },
      {
        name: 'Otap',
        coverPhoto: cover4,
      },
      {
        name: 'Piaya',
        coverPhoto: cover1,
      },
    ], // Fetch from contentful
  },
  {
    name: 'Pasalubong',
    desc: 'Bread & Butter Delicacies',
    details: 'Siguradong damang dama ang iyong alala at ngiting dala kasama ang Bread & Butter delicacies at pasalubong',
    matteImage: matte2,
    illustration: pasalubong,
    color: 'plum',
    featuredItems: [
      {
        name: 'Piyaya',
        coverPhoto: cover1,
      },
      {
        name: 'Wheat Pandesal',
        coverPhoto: cover2,
      },
      {
        name: 'Cinnamon Toast',
        coverPhoto: cover3,
      },
      {
        name: 'Otap',
        coverPhoto: cover4,
      },
      {
        name: 'Piaya',
        coverPhoto: cover1,
      },
    ],
  },
  {
    name: 'Merienda',
    desc: 'Bread & Butter Snacks',
    details: 'Gawing extra special ang merienda ng pamilya at bisita kasama ang makulay at masarap na tinapay mula sa Bread & Butter!',
    matteImage: matte3,
    illustration: merienda,
    color: 'neon-carrot',
    featuredItems: [
      {
        name: 'Piyaya',
        coverPhoto: cover1,
      },
      {
        name: 'Wheat Pandesal',
        coverPhoto: cover2,
      },
      {
        name: 'Cinnamon Toast',
        coverPhoto: cover3,
      },
      {
        name: 'Otap',
        coverPhoto: cover4,
      },
      {
        name: 'Piaya',
        coverPhoto: cover1,
      },
    ],
  },
  {
    name: 'Almusal',
    desc: 'Bread & Butter Breakfast',
    details: 'Kumpleto ang almusal kasama ang mga paborito niyong tinapay mula sa Bread & Butter!',
    matteImage: matte4,
    illustration: almusal,
    color: 'tango',
    featuredItems: [
      {
        name: 'Piyaya',
        coverPhoto: cover1,
      },
      {
        name: 'Wheat Pandesal',
        coverPhoto: cover2,
      },
      {
        name: 'Cinnamon Toast',
        coverPhoto: cover3,
      },
      {
        name: 'Otap',
        coverPhoto: cover4,
      },
      {
        name: 'Piaya',
        coverPhoto: cover1,
      },
    ],
  },
]
