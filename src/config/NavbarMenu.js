export default [
  {
    name: 'Grow with Us',
    path: '/grow',
    cta: false,
    target: '',
    rel: '',
  },
  {
    name: 'About Us',
    path: '/about',
    cta: false,
  },
  {
    name: 'Our Blog',
    path: 'https://medium.com/@breadandbutterbakery',
    cta: false,
    target: '_blank',
    rel: 'noopener noreferrer',
  },
  {
    name: 'Careers',
    path: '/careers',
    cta: false,
    target: '',
    rel: '',
  },
  {
    name: 'Contact Us',
    path: '/',
    cta: true,
    target: '',
    rel: '',
  },
]
