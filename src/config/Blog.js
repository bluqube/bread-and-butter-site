import coverPhoto from '../assets/cover-photo.jpg'

export default [
  {
    coverPhoto: 'https://cdn-images-1.medium.com/max/1600/1*nvv7qv5aQSZ3KlDuW-XbtA.jpeg',
    date: '',
    title: 'Bread & Butter Celebrates 11th Year in Cabatuan!',
    description: 'CABATUAN ILOILO, It was a better and brighter day for Ilonggos as Bread & Butter re-open its store in Cabatuan with its new and refreshing design concept.',
    featured: true,
    blogLink: 'https://medium.com/@breadandbutterbakery/bread-butter-celebrates-11th-year-in-cabatuan-654cdcd45457',
  },
  {
    coverPhoto:'https://cdn-images-1.medium.com/max/800/1*ddVNlYg4kAiuxcqU1bZAOA.jpeg',
    date: '',
    title: 'Antiqueno excels in Bread & Butter Franchise Award',
    description: 'Pandananon Ms. Francesca Carmelita Sanchez recently bagged the Franchisee of the Year Award from Bread & Butter Bakeshop (BB). Having operated her store BB Pandan for more than 10 years, Sanchez has consistently delivered excellent sales and met operations targets in the CY 2018. Sanchez won a Trip to Hong Kong/Singapore. She was awarded last March 2, 2019 at the Ati-atihan Festival Hotel in Kalibo, Aklan where the nationally-recognized company is headquartered.',
    featured: false,
    blogLink: 'https://medium.com/@breadandbutterbakery/antiqueno-excels-in-bread-butter-franchise-award-5b40efb8097b',
  },
  {
    coverPhoto: 'https://cdn-images-1.medium.com/max/800/1*uiFdvJH8xhIP1ffo5jNG7w.jpeg',
    date: '',
    title: 'Bread & Butter Celebrates 32nd Store Anniversary',
    description: 'Bread & Butter was first to serve Pan de Sal and Spanish bread in a small store located on Osmenia Avenue, Kalibo, Aklan. It is the first store which is now known as BB Estancia where the Bread & Butter started its success stories on baking and selling experience way back in 1985. Able to live for three decades overcoming challenges on its business operations is a milestone for Bread & Butter Estancia to celebrate because not all can stay longer like it does.',
    featured: false,
    blogLink: 'https://medium.com/@breadandbutterbakery/bread-butter-celebrates-33nd-store-anniversary-1414d55d360e',
  },

]
