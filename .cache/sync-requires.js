// prefer default export if available
const preferDefault = m => m && m.default || m


exports.layouts = {
  "layout---index": preferDefault(require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/layouts/index.js"))
}

exports.components = {
  "component---src-pages-404-js": preferDefault(require("/home/bashdev7/Repositories/bread-and-butter-site/src/pages/404.js")),
  "component---src-pages-index-js": preferDefault(require("/home/bashdev7/Repositories/bread-and-butter-site/src/pages/index.js")),
  "component---src-pages-grow-js": preferDefault(require("/home/bashdev7/Repositories/bread-and-butter-site/src/pages/grow.js")),
  "component---src-pages-products-js": preferDefault(require("/home/bashdev7/Repositories/bread-and-butter-site/src/pages/products.js"))
}

exports.json = {
  "layout-index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "404.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/404.json"),
  "layout-index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/index.json"),
  "layout-index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "grow.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/grow.json"),
  "layout-index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "products.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/products.json"),
  "layout-index.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "404-html.json": require("/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/404-html.json")
}