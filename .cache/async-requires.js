// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-pages-404-js": require("gatsby-module-loader?name=component---src-pages-404-js!/home/bashdev7/Repositories/bread-and-butter-site/src/pages/404.js"),
  "component---src-pages-index-js": require("gatsby-module-loader?name=component---src-pages-index-js!/home/bashdev7/Repositories/bread-and-butter-site/src/pages/index.js"),
  "component---src-pages-grow-js": require("gatsby-module-loader?name=component---src-pages-grow-js!/home/bashdev7/Repositories/bread-and-butter-site/src/pages/grow.js"),
  "component---src-pages-products-js": require("gatsby-module-loader?name=component---src-pages-products-js!/home/bashdev7/Repositories/bread-and-butter-site/src/pages/products.js")
}

exports.json = {
  "layout-index.json": require("gatsby-module-loader?name=path---!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "404.json": require("gatsby-module-loader?name=path---404!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/404.json"),
  "layout-index.json": require("gatsby-module-loader?name=path---!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "index.json": require("gatsby-module-loader?name=path---index!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/index.json"),
  "layout-index.json": require("gatsby-module-loader?name=path---!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "grow.json": require("gatsby-module-loader?name=path---grow!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/grow.json"),
  "layout-index.json": require("gatsby-module-loader?name=path---!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "products.json": require("gatsby-module-loader?name=path---products!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/products.json"),
  "layout-index.json": require("gatsby-module-loader?name=path---!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/layout-index.json"),
  "404-html.json": require("gatsby-module-loader?name=path---404-html!/home/bashdev7/Repositories/bread-and-butter-site/.cache/json/404-html.json")
}

exports.layouts = {
  "layout---index": require("gatsby-module-loader?name=component---src-layouts-index-js!/home/bashdev7/Repositories/bread-and-butter-site/.cache/layouts/index.js")
}